﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OglasnikApp {
    class Oglas {
        public Oglas(string naslov, string sadrzaj) {
            Naslov = naslov;
            Sadrzaj = sadrzaj;
        }

        public string Naslov { get; set; }
        public string Sadrzaj { get; set; }
    }
}
