﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OglasnikApp {
    public partial class Form1 : Form {
        private Oglasnik oglasnik;

        public Form1() {
            InitializeComponent();
            oglasnik = new Oglasnik();
        }

        private void Form1_Load(object sender, EventArgs e) {

        }

        private void button1_Click(object sender, EventArgs e) {
            oglasnik.DodajOglas(
                new Oglas(textBox1.Text, textBox2.Text)
            );

            textBox3.Text = "";
            foreach (var oglas in oglasnik.SviOglasi) {
                textBox3.Text += oglas.Naslov + " " + oglas.Sadrzaj + "\n";
            }
        }
    }
}
