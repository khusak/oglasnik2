﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OglasnikApp {
    class Oglasnik {
        private List<Oglas> oglasi;

        public Oglasnik() {
            oglasi = new List<Oglas>();
        }

        public void DodajOglas(Oglas oglas) {
            oglasi.Add(oglas);
        }

        public List<Oglas> SviOglasi { get => oglasi; }


    }
}
